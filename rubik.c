#include "rubik.h"

int main(int argc, char *argv[]) {
    Rubik rubik;
    rubik_init(rubik);
    main_loop(rubik);
    return 0;
}

void main_loop(Rubik rubik) {
    char a, b;
    for (;;) {
        printf(RESET);
        a=toupper(getchar());
        if (a == 'Q') break;
        if (a != 10) {
            b=toupper(getchar());
            if (b != 10) getchar();
            rotate(rubik, a, b);
        }
        rubik_print(rubik);
    }
}

void rubik_init(Rubik rubik) {
    int i,n;
    char c[7] = " RGOBYW";

    for (n=1; n<=7; n++)
        for (i=1; i<=10; i++)
            rubik[n][i] = c[n];
}

void rotate(Rubik rubik, char a, char b){
    int i = b == '2' ? 2 : b == '\'' || b == toupper('i') ? 3 : 1;
    int j;
    for (j=0; j<i; j++)
        if (a == 'F') F(rubik);
        else if (a == 'B') B(rubik);
        else if (a == 'L') L(rubik);
        else if (a == 'R') R(rubik);
        else if (a == 'U') U(rubik);
        else if (a == 'D') D(rubik);
        else;
}

void rubik_print(Rubik rubik) {
    int i,j,k;

    for (i=1; i<=9; i++)
        if (i%3==0) {
            print_tile(rubik,5,i);
            printf("\n");
        }
        else if (i%3==1) {
            printf("    ");
            print_tile(rubik,5,i);
        }
        else
            print_tile(rubik,5,i);

    printf("\n");

    for (k=1; k<=3; k++) {
        for (i=3; i<=6; i++) {
            for (j=k*3-2; j<=k*3; j++)
                print_tile(rubik, i%4+1, j);
            printf(" ");
        }
        printf("\n");
    }

    printf("\n");

    for (i=1; i<=9; i++)
        if (i%3==0) {
            print_tile(rubik, 6, i);
            printf("\n");
        }
        else if (i%3==1) {
            printf("    ");
            print_tile(rubik, 6, i);
        }
        else
            print_tile(rubik, 6, i);

}

void print_tile(Rubik rubik, int a, int b) {
    switch (rubik[a][b]) {
      case 'R':
        printf(RE "@");
        break;
      case 'O':
        printf(O "@");
        break;
      case 'B':
        printf(BL "@");
        break;
      case 'G':
        printf(G "@");
        break;
      case 'Y':
        printf(Y "@");
        break;
      case 'W':
        printf(W "@");
        break;
      default:
        exit(1);
    }
}
