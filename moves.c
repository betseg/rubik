#include "rubik.h"

#define move(a, b, c, d) \
      temp[c][d] = rubik[a][b]

#define move_f(a) \
    move(a, 1, a, 3); \
    move(a, 2, a, 6); \
    move(a, 3, a, 9); \
    move(a, 4, a, 2); \
    move(a, 6, a, 8); \
    move(a, 7, a, 1); \
    move(a, 8, a, 4); \
    move(a, 9, a, 7)

void F(Rubik rubik) {
    Rubik temp;
    memcpy(temp, rubik, sizeof(Rubik));
    move_f(1);
    move(5, 7, 2, 1);
    move(5, 8, 2, 4);
    move(5, 9, 2, 7);
    move(2, 1, 6, 1);
    move(2, 4, 6, 2);
    move(2, 7, 6, 3);
    move(6, 1, 4, 3);
    move(6, 2, 4, 6);
    move(6, 3, 4, 9);
    move(4, 3, 5, 7);
    move(4, 6, 5, 8);
    move(4, 9, 5, 9);
    memcpy(rubik, temp, sizeof(Rubik));
}

void B(Rubik rubik) {
    Rubik temp;
    memcpy(temp, rubik, sizeof(Rubik));
    move_f(3);
    move(2, 3, 5, 1);
    move(2, 6, 5, 2);
    move(2, 9, 5, 3);
    move(4, 1, 6, 7);
    move(4, 4, 6, 8);
    move(4, 7, 6, 9);
    move(5, 1, 4, 7);
    move(5, 2, 4, 4);
    move(5, 3, 4, 1);
    move(6, 7, 2, 9);
    move(6, 8, 2, 6);
    move(6, 9, 2, 3);
    memcpy(rubik, temp, sizeof(Rubik));
}

void L(Rubik rubik) {
    Rubik temp;
    memcpy(temp, rubik, sizeof(Rubik));
    move_f(4);
    move(1, 1, 6, 1);
    move(1, 4, 6, 4);
    move(1, 7, 6, 7);
    move(3, 3, 5, 7);
    move(3, 6, 5, 4);
    move(3, 9, 5, 1);
    move(5, 1, 1, 1);
    move(5, 4, 1, 4);
    move(5, 7, 1, 7);
    move(6, 1, 3, 9);
    move(6, 4, 3, 6);
    move(6, 7, 3, 3);
    memcpy(rubik, temp, sizeof(Rubik));
}

void R(Rubik rubik) {
    Rubik temp;
    memcpy(temp, rubik, sizeof(Rubik));
    move_f(2);
    move(1, 3, 5, 3);
    move(1, 6, 5, 6);
    move(1, 9, 5, 9);
    move(6, 3, 1, 3);
    move(6, 6, 1, 6);
    move(6, 9, 1, 9);
    move(5, 3, 3, 7);
    move(5, 6, 3, 4);
    move(5, 9, 3, 1);
    move(3, 1, 6, 9);
    move(3, 4, 6, 6);
    move(3, 7, 6, 3);
    memcpy(rubik, temp, sizeof(Rubik));
}

void U(Rubik rubik) {
    Rubik temp;
    memcpy(temp, rubik, sizeof(Rubik));
    move_f(5);
    move(1, 1, 4, 1);
    move(1, 2, 4, 2);
    move(1, 3, 4, 3);
    move(2, 1, 1, 1);
    move(2, 2, 1, 2);
    move(2, 3, 1, 3);
    move(3, 1, 2, 1);
    move(3, 2, 2, 2);
    move(3, 3, 2, 3);
    move(4, 1, 3, 1);
    move(4, 2, 3, 2);
    move(4, 3, 3, 3);
    memcpy(rubik, temp, sizeof(Rubik));
}

void D(Rubik rubik) {
    Rubik temp;
    memcpy(temp, rubik, sizeof(Rubik));
    move_f(6);
    move(1, 7, 2, 7);
    move(1, 8, 2, 8);
    move(1, 9, 2, 9);
    move(2, 7, 3, 7);
    move(2, 8, 3, 8);
    move(2, 9, 3, 9);
    move(3, 7, 4, 7);
    move(3, 8, 4, 8);
    move(3, 9, 4, 9);
    move(4, 7, 1, 7);
    move(4, 8, 1, 8);
    move(4, 9, 1, 9);
    memcpy(rubik, temp, sizeof(Rubik));
}
