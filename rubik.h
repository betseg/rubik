#ifndef _RUBIK_H_
# define _RUBIK_H_

# include <stdio.h>
# include <stdlib.h>
# include <stdbool.h>
# include <string.h>
# include <ctype.h>

typedef char Tile;
typedef Tile Face[10];
typedef Face Rubik[7];

void main_loop(Rubik);
void rubik_init(Rubik);
void rubik_print(Rubik);
void rotate(Rubik, char, char);
void print_tile(Rubik, int, int);

void F(Rubik);
void B(Rubik);
void L(Rubik);
void R(Rubik);
void U(Rubik);
void D(Rubik);

# define RE "\x1B[31m"
# define G "\x1B[32m"
# define Y "\x1B[33m"
# define BL "\x1B[34m"
# define O "\x1B[35m"
# define W "\x1B[37m"
# define RESET "\x1B[0m"

#endif //_RUBIK_H_
